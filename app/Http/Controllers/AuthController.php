<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }

    public function welcome(Request $request){
        $namadpn = $request['depan'];
        $namablkng = $request['belakang'];

        return view('halaman.welcome', compact('namadpn', 'namablkng'));
    }
}
